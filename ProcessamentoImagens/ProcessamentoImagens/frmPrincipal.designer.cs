﻿namespace ProcessamentoImagens
{
    partial class frmPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictBoxImg1 = new System.Windows.Forms.PictureBox();
            this.pictBoxImg2 = new System.Windows.Forms.PictureBox();
            this.btnAbrirImagem = new System.Windows.Forms.Button();
            this.btnLimpar = new System.Windows.Forms.Button();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.btnLuminanciaSemDMA = new System.Windows.Forms.Button();
            this.btnLuminanciaComDMA = new System.Windows.Forms.Button();
            this.btnNegativoComDMA = new System.Windows.Forms.Button();
            this.btnNegativoSemDMA = new System.Windows.Forms.Button();
            this.SobelsmDma = new System.Windows.Forms.Button();
            this.PrewittsmDMA = new System.Windows.Forms.Button();
            this.RobertssmDMA = new System.Windows.Forms.Button();
            this.btnInverteCanais_com_DMA = new System.Windows.Forms.Button();
            this.btnInverteCanais_sem_DMA = new System.Windows.Forms.Button();
            this.btnGirar90_sem_DMA = new System.Windows.Forms.Button();
            this.btnGirar90_com_DMA = new System.Windows.Forms.Button();
            this.MediaVisinho5x5 = new System.Windows.Forms.Button();
            this.RobertscmDMA = new System.Windows.Forms.Button();
            this.PrewittcmDMA = new System.Windows.Forms.Button();
            this.SobelcmDMA = new System.Windows.Forms.Button();
            this.MediaKVizinhos9 = new System.Windows.Forms.Button();
            this.Mediana5X5 = new System.Windows.Forms.Button();
            this.AfinamentoZSsmDMA = new System.Windows.Forms.Button();
            this.AfinamentoZScmDMA = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictBoxImg1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictBoxImg2)).BeginInit();
            this.SuspendLayout();
            // 
            // pictBoxImg1
            // 
            this.pictBoxImg1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.pictBoxImg1.Location = new System.Drawing.Point(5, 6);
            this.pictBoxImg1.Name = "pictBoxImg1";
            this.pictBoxImg1.Size = new System.Drawing.Size(600, 500);
            this.pictBoxImg1.TabIndex = 102;
            this.pictBoxImg1.TabStop = false;
            // 
            // pictBoxImg2
            // 
            this.pictBoxImg2.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.pictBoxImg2.Location = new System.Drawing.Point(611, 6);
            this.pictBoxImg2.Name = "pictBoxImg2";
            this.pictBoxImg2.Size = new System.Drawing.Size(600, 500);
            this.pictBoxImg2.TabIndex = 105;
            this.pictBoxImg2.TabStop = false;
            // 
            // btnAbrirImagem
            // 
            this.btnAbrirImagem.Location = new System.Drawing.Point(5, 512);
            this.btnAbrirImagem.Name = "btnAbrirImagem";
            this.btnAbrirImagem.Size = new System.Drawing.Size(101, 23);
            this.btnAbrirImagem.TabIndex = 106;
            this.btnAbrirImagem.Text = "Abrir Imagem";
            this.btnAbrirImagem.UseVisualStyleBackColor = true;
            this.btnAbrirImagem.Click += new System.EventHandler(this.btnAbrirImagem_Click);
            // 
            // btnLimpar
            // 
            this.btnLimpar.Location = new System.Drawing.Point(112, 512);
            this.btnLimpar.Name = "btnLimpar";
            this.btnLimpar.Size = new System.Drawing.Size(101, 23);
            this.btnLimpar.TabIndex = 107;
            this.btnLimpar.Text = "Limpar";
            this.btnLimpar.UseVisualStyleBackColor = true;
            this.btnLimpar.Click += new System.EventHandler(this.btnLimpar_Click);
            // 
            // btnLuminanciaSemDMA
            // 
            this.btnLuminanciaSemDMA.Location = new System.Drawing.Point(219, 512);
            this.btnLuminanciaSemDMA.Name = "btnLuminanciaSemDMA";
            this.btnLuminanciaSemDMA.Size = new System.Drawing.Size(208, 23);
            this.btnLuminanciaSemDMA.TabIndex = 108;
            this.btnLuminanciaSemDMA.Text = "Luminância sem DMA";
            this.btnLuminanciaSemDMA.UseVisualStyleBackColor = true;
            this.btnLuminanciaSemDMA.Click += new System.EventHandler(this.btnLuminanciaSemDMA_Click);
            // 
            // btnLuminanciaComDMA
            // 
            this.btnLuminanciaComDMA.Location = new System.Drawing.Point(219, 541);
            this.btnLuminanciaComDMA.Name = "btnLuminanciaComDMA";
            this.btnLuminanciaComDMA.Size = new System.Drawing.Size(208, 23);
            this.btnLuminanciaComDMA.TabIndex = 109;
            this.btnLuminanciaComDMA.Text = "Luminância com DMA";
            this.btnLuminanciaComDMA.UseVisualStyleBackColor = true;
            this.btnLuminanciaComDMA.Click += new System.EventHandler(this.btnLuminanciaComDMA_Click);
            // 
            // btnNegativoComDMA
            // 
            this.btnNegativoComDMA.Location = new System.Drawing.Point(433, 541);
            this.btnNegativoComDMA.Name = "btnNegativoComDMA";
            this.btnNegativoComDMA.Size = new System.Drawing.Size(208, 23);
            this.btnNegativoComDMA.TabIndex = 111;
            this.btnNegativoComDMA.Text = "Negativo com DMA";
            this.btnNegativoComDMA.UseVisualStyleBackColor = true;
            this.btnNegativoComDMA.Click += new System.EventHandler(this.btnNegativoComDMA_Click);
            // 
            // btnNegativoSemDMA
            // 
            this.btnNegativoSemDMA.Location = new System.Drawing.Point(433, 512);
            this.btnNegativoSemDMA.Name = "btnNegativoSemDMA";
            this.btnNegativoSemDMA.Size = new System.Drawing.Size(208, 23);
            this.btnNegativoSemDMA.TabIndex = 110;
            this.btnNegativoSemDMA.Text = "Negativo sem DMA";
            this.btnNegativoSemDMA.UseVisualStyleBackColor = true;
            this.btnNegativoSemDMA.Click += new System.EventHandler(this.btnNegativoSemDMA_Click);
            // 
            // SobelsmDma
            // 
            this.SobelsmDma.Location = new System.Drawing.Point(647, 512);
            this.SobelsmDma.Name = "SobelsmDma";
            this.SobelsmDma.Size = new System.Drawing.Size(208, 23);
            this.SobelsmDma.TabIndex = 112;
            this.SobelsmDma.Text = "Sobel Sem DMA";
            this.SobelsmDma.UseVisualStyleBackColor = true;
            this.SobelsmDma.Click += new System.EventHandler(this.SobelsmDma_Click);
            // 
            // PrewittsmDMA
            // 
            this.PrewittsmDMA.Location = new System.Drawing.Point(647, 541);
            this.PrewittsmDMA.Name = "PrewittsmDMA";
            this.PrewittsmDMA.Size = new System.Drawing.Size(208, 23);
            this.PrewittsmDMA.TabIndex = 113;
            this.PrewittsmDMA.Text = "Prewitt Sem DMA";
            this.PrewittsmDMA.UseVisualStyleBackColor = true;
            this.PrewittsmDMA.Click += new System.EventHandler(this.PrewittsmDMA_Click);
            // 
            // RobertssmDMA
            // 
            this.RobertssmDMA.Location = new System.Drawing.Point(647, 570);
            this.RobertssmDMA.Name = "RobertssmDMA";
            this.RobertssmDMA.Size = new System.Drawing.Size(208, 23);
            this.RobertssmDMA.TabIndex = 114;
            this.RobertssmDMA.Text = "Roberts Sem DMA";
            this.RobertssmDMA.UseVisualStyleBackColor = true;
            this.RobertssmDMA.Click += new System.EventHandler(this.RobertssmDMA_Click);
            // 
            // btnInverteCanais_com_DMA
            // 
            this.btnInverteCanais_com_DMA.Location = new System.Drawing.Point(433, 598);
            this.btnInverteCanais_com_DMA.Name = "btnInverteCanais_com_DMA";
            this.btnInverteCanais_com_DMA.Size = new System.Drawing.Size(208, 23);
            this.btnInverteCanais_com_DMA.TabIndex = 119;
            this.btnInverteCanais_com_DMA.Text = "Inverte R e B com DMA";
            this.btnInverteCanais_com_DMA.UseVisualStyleBackColor = true;
            this.btnInverteCanais_com_DMA.Click += new System.EventHandler(this.btnInverteCanais_com_DMA_Click);
            // 
            // btnInverteCanais_sem_DMA
            // 
            this.btnInverteCanais_sem_DMA.Location = new System.Drawing.Point(433, 569);
            this.btnInverteCanais_sem_DMA.Name = "btnInverteCanais_sem_DMA";
            this.btnInverteCanais_sem_DMA.Size = new System.Drawing.Size(208, 23);
            this.btnInverteCanais_sem_DMA.TabIndex = 118;
            this.btnInverteCanais_sem_DMA.Text = "Inverte R e B sem DMA";
            this.btnInverteCanais_sem_DMA.UseVisualStyleBackColor = true;
            this.btnInverteCanais_sem_DMA.Click += new System.EventHandler(this.btnInverteCanais_sem_DMA_Click);
            // 
            // btnGirar90_sem_DMA
            // 
            this.btnGirar90_sem_DMA.Location = new System.Drawing.Point(219, 569);
            this.btnGirar90_sem_DMA.Name = "btnGirar90_sem_DMA";
            this.btnGirar90_sem_DMA.Size = new System.Drawing.Size(208, 23);
            this.btnGirar90_sem_DMA.TabIndex = 117;
            this.btnGirar90_sem_DMA.Text = "Girar 90 sem DMA";
            this.btnGirar90_sem_DMA.UseVisualStyleBackColor = true;
            this.btnGirar90_sem_DMA.Click += new System.EventHandler(this.btnGirar90_sem_DMA_Click);
            // 
            // btnGirar90_com_DMA
            // 
            this.btnGirar90_com_DMA.Location = new System.Drawing.Point(219, 598);
            this.btnGirar90_com_DMA.Name = "btnGirar90_com_DMA";
            this.btnGirar90_com_DMA.Size = new System.Drawing.Size(208, 23);
            this.btnGirar90_com_DMA.TabIndex = 116;
            this.btnGirar90_com_DMA.Text = "Girar 90 com DMA";
            this.btnGirar90_com_DMA.UseVisualStyleBackColor = true;
            this.btnGirar90_com_DMA.Click += new System.EventHandler(this.btnGirar90_com_DMA_Click);
            // 
            // MediaVisinho5x5
            // 
            this.MediaVisinho5x5.Location = new System.Drawing.Point(1075, 541);
            this.MediaVisinho5x5.Name = "MediaVisinho5x5";
            this.MediaVisinho5x5.Size = new System.Drawing.Size(136, 23);
            this.MediaVisinho5x5.TabIndex = 120;
            this.MediaVisinho5x5.Text = "Média de Visinhos 5X5";
            this.MediaVisinho5x5.UseVisualStyleBackColor = true;
            this.MediaVisinho5x5.Click += new System.EventHandler(this.MediaVisinho5x5_Click);
            // 
            // RobertscmDMA
            // 
            this.RobertscmDMA.Location = new System.Drawing.Point(861, 570);
            this.RobertscmDMA.Name = "RobertscmDMA";
            this.RobertscmDMA.Size = new System.Drawing.Size(208, 23);
            this.RobertscmDMA.TabIndex = 123;
            this.RobertscmDMA.Text = "Roberts Com DMA";
            this.RobertscmDMA.UseVisualStyleBackColor = true;
            this.RobertscmDMA.Click += new System.EventHandler(this.RobertscmDMA_Click);
            // 
            // PrewittcmDMA
            // 
            this.PrewittcmDMA.Location = new System.Drawing.Point(861, 541);
            this.PrewittcmDMA.Name = "PrewittcmDMA";
            this.PrewittcmDMA.Size = new System.Drawing.Size(208, 23);
            this.PrewittcmDMA.TabIndex = 122;
            this.PrewittcmDMA.Text = "Prewitt Com DMA";
            this.PrewittcmDMA.UseVisualStyleBackColor = true;
            this.PrewittcmDMA.Click += new System.EventHandler(this.PrewittcmDMA_Click);
            // 
            // SobelcmDMA
            // 
            this.SobelcmDMA.Location = new System.Drawing.Point(861, 512);
            this.SobelcmDMA.Name = "SobelcmDMA";
            this.SobelcmDMA.Size = new System.Drawing.Size(208, 23);
            this.SobelcmDMA.TabIndex = 121;
            this.SobelcmDMA.Text = "Sobel Com DMA";
            this.SobelcmDMA.UseVisualStyleBackColor = true;
            this.SobelcmDMA.Click += new System.EventHandler(this.SobelcmDMA_Click);
            // 
            // MediaKVizinhos9
            // 
            this.MediaKVizinhos9.Location = new System.Drawing.Point(1075, 570);
            this.MediaKVizinhos9.Name = "MediaKVizinhos9";
            this.MediaKVizinhos9.Size = new System.Drawing.Size(136, 23);
            this.MediaKVizinhos9.TabIndex = 127;
            this.MediaKVizinhos9.Text = "Média K-Vizinhos 9";
            this.MediaKVizinhos9.UseVisualStyleBackColor = true;
            this.MediaKVizinhos9.Click += new System.EventHandler(this.MediaKVizinhos9_Click);
            // 
            // Mediana5X5
            // 
            this.Mediana5X5.Location = new System.Drawing.Point(1075, 512);
            this.Mediana5X5.Name = "Mediana5X5";
            this.Mediana5X5.Size = new System.Drawing.Size(136, 23);
            this.Mediana5X5.TabIndex = 126;
            this.Mediana5X5.Text = "Mediana 5X5";
            this.Mediana5X5.UseVisualStyleBackColor = true;
            this.Mediana5X5.Click += new System.EventHandler(this.Mediana5X5_Click);
            // 
            // AfinamentoZSsmDMA
            // 
            this.AfinamentoZSsmDMA.Location = new System.Drawing.Point(647, 598);
            this.AfinamentoZSsmDMA.Name = "AfinamentoZSsmDMA";
            this.AfinamentoZSsmDMA.Size = new System.Drawing.Size(208, 23);
            this.AfinamentoZSsmDMA.TabIndex = 128;
            this.AfinamentoZSsmDMA.Text = "Afinamento Zhang Suen Sem DMA";
            this.AfinamentoZSsmDMA.UseVisualStyleBackColor = true;
            this.AfinamentoZSsmDMA.Click += new System.EventHandler(this.AfinamentoZSsmDMA_Click);
            // 
            // AfinamentoZScmDMA
            // 
            this.AfinamentoZScmDMA.Location = new System.Drawing.Point(861, 599);
            this.AfinamentoZScmDMA.Name = "AfinamentoZScmDMA";
            this.AfinamentoZScmDMA.Size = new System.Drawing.Size(208, 23);
            this.AfinamentoZScmDMA.TabIndex = 129;
            this.AfinamentoZScmDMA.Text = "Afinamento Zhang Suen Com DMA";
            this.AfinamentoZScmDMA.UseVisualStyleBackColor = true;
            this.AfinamentoZScmDMA.Click += new System.EventHandler(this.AfinamentoZScmDMA_Click);
            // 
            // frmPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1220, 629);
            this.Controls.Add(this.AfinamentoZScmDMA);
            this.Controls.Add(this.AfinamentoZSsmDMA);
            this.Controls.Add(this.MediaKVizinhos9);
            this.Controls.Add(this.Mediana5X5);
            this.Controls.Add(this.RobertscmDMA);
            this.Controls.Add(this.PrewittcmDMA);
            this.Controls.Add(this.SobelcmDMA);
            this.Controls.Add(this.MediaVisinho5x5);
            this.Controls.Add(this.btnInverteCanais_com_DMA);
            this.Controls.Add(this.btnInverteCanais_sem_DMA);
            this.Controls.Add(this.btnGirar90_sem_DMA);
            this.Controls.Add(this.btnGirar90_com_DMA);
            this.Controls.Add(this.RobertssmDMA);
            this.Controls.Add(this.PrewittsmDMA);
            this.Controls.Add(this.SobelsmDma);
            this.Controls.Add(this.btnNegativoComDMA);
            this.Controls.Add(this.btnNegativoSemDMA);
            this.Controls.Add(this.btnLuminanciaComDMA);
            this.Controls.Add(this.btnLuminanciaSemDMA);
            this.Controls.Add(this.btnLimpar);
            this.Controls.Add(this.btnAbrirImagem);
            this.Controls.Add(this.pictBoxImg2);
            this.Controls.Add(this.pictBoxImg1);
            this.Name = "frmPrincipal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Formulário Principal";
            ((System.ComponentModel.ISupportInitialize)(this.pictBoxImg1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictBoxImg2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictBoxImg1;
        private System.Windows.Forms.PictureBox pictBoxImg2;
        private System.Windows.Forms.Button btnAbrirImagem;
        private System.Windows.Forms.Button btnLimpar;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.Button btnLuminanciaSemDMA;
        private System.Windows.Forms.Button btnLuminanciaComDMA;
        private System.Windows.Forms.Button btnNegativoComDMA;
        private System.Windows.Forms.Button btnNegativoSemDMA;
        private System.Windows.Forms.Button SobelsmDma;
        private System.Windows.Forms.Button PrewittsmDMA;
        private System.Windows.Forms.Button RobertssmDMA;
        private System.Windows.Forms.Button btnInverteCanais_com_DMA;
        private System.Windows.Forms.Button btnInverteCanais_sem_DMA;
        private System.Windows.Forms.Button btnGirar90_sem_DMA;
        private System.Windows.Forms.Button btnGirar90_com_DMA;
        private System.Windows.Forms.Button MediaVisinho5x5;
        private System.Windows.Forms.Button RobertscmDMA;
        private System.Windows.Forms.Button PrewittcmDMA;
        private System.Windows.Forms.Button SobelcmDMA;
        private System.Windows.Forms.Button MediaKVizinhos9;
        private System.Windows.Forms.Button Mediana5X5;
        private System.Windows.Forms.Button AfinamentoZSsmDMA;
        private System.Windows.Forms.Button AfinamentoZScmDMA;
    }
}

