﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Imaging;

namespace ProcessamentoImagens
{
    public partial class frmPrincipal : Form
    {
        private Image image;
        private Bitmap imageBitmap;

        public frmPrincipal()
        {
            InitializeComponent();
            pictBoxImg1.SizeMode = PictureBoxSizeMode.StretchImage;
            pictBoxImg2.SizeMode = PictureBoxSizeMode.StretchImage;
        }

        private void btnAbrirImagem_Click(object sender, EventArgs e)
        {
            openFileDialog.FileName = "";
            openFileDialog.Filter = "Arquivos de Imagem (*.jpg;*.gif;*.bmp;*.png)|*.jpg;*.gif;*.bmp;*.png";
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                image = Image.FromFile(openFileDialog.FileName);
                pictBoxImg1.Image = image;
            }
        }

        private void btnLimpar_Click(object sender, EventArgs e)
        {
            pictBoxImg1.Image = null;
            pictBoxImg2.Image = null;
        }

        private void btnLuminanciaSemDMA_Click(object sender, EventArgs e)
        {
            Bitmap imgDest = new Bitmap(image);
            imageBitmap = (Bitmap)image;
            Filtros.convert_to_gray(imageBitmap, imgDest);
            pictBoxImg2.Image = imgDest;
        }

        private void btnLuminanciaComDMA_Click(object sender, EventArgs e)
        {
            Bitmap imgDest = new Bitmap(image);
            imageBitmap = (Bitmap)image;
            Filtros.convert_to_grayDMA(imageBitmap, imgDest);
            pictBoxImg2.Image = imgDest;
        }

        private void btnNegativoSemDMA_Click(object sender, EventArgs e)
        {
            Bitmap imgDest = new Bitmap(image);
            imageBitmap = (Bitmap)image;
            Filtros.negativo(imageBitmap, imgDest);
            pictBoxImg2.Image = imgDest;
        }

        private void btnNegativoComDMA_Click(object sender, EventArgs e)
        {
            Bitmap imgDest = new Bitmap(image);
            imageBitmap = (Bitmap)image;
            Filtros.negativoDMA(imageBitmap, imgDest);
            pictBoxImg2.Image = imgDest;
        }

        private void SobelsmDma_Click(object sender, EventArgs e)
        {
            Bitmap imgDest = new Bitmap(image);
            imageBitmap = (Bitmap)image;
            Filtros.generate_lines(imageBitmap, imgDest);
            pictBoxImg2.Image = imgDest;
        }

        private void PrewittsmDMA_Click(object sender, EventArgs e)
        {
            Bitmap imgDest = new Bitmap(image);
            imageBitmap = (Bitmap)image;
            Filtros.generate_lines_Prewitt(imageBitmap, imgDest);
            pictBoxImg2.Image = imgDest;
        }

        private void RobertssmDMA_Click(object sender, EventArgs e)
        {
            Bitmap imgDest = new Bitmap(image);
            imageBitmap = (Bitmap)image;
            Filtros.generate_lines_Roberts(imageBitmap, imgDest);
            pictBoxImg2.Image = imgDest;
        }

        private void btnGirar90_sem_DMA_Click(object sender, EventArgs e)
        {
            Bitmap imgDest = new Bitmap(image);
            imageBitmap = (Bitmap)image;
            imgDest = Filtros.Gira90Grau(imageBitmap, imgDest);
            pictBoxImg2.Image = imgDest;
        }

        private void btnGirar90_com_DMA_Click(object sender, EventArgs e)
        {
            Bitmap imgDest;
            imageBitmap = new Bitmap(image);
            imgDest = new Bitmap(imageBitmap.Height, imageBitmap.Width);
            Filtros.Girar90ComMemoria(imageBitmap, imgDest);
            pictBoxImg2.Image = imgDest;//raind - (c + 1) * stride 2 + l * 3
        }

        private void btnInverteCanais_sem_DMA_Click(object sender, EventArgs e)
        {
            Bitmap imgDest = new Bitmap(image);
            imageBitmap = (Bitmap)image;
            Filtros.InverteRandB_sem_memoria(imageBitmap, imgDest);
            pictBoxImg2.Image = imgDest;
        }

        private void btnInverteCanais_com_DMA_Click(object sender, EventArgs e)
        {
            Bitmap imgDest = new Bitmap(image);
            imageBitmap = (Bitmap)image;
            Filtros.InverteRandB_com_memoria(imageBitmap, imgDest);
            pictBoxImg2.Image = imgDest;
        }

        private void MediaVisinho5x5_Click(object sender, EventArgs e)
        {
            Bitmap imgDest = new Bitmap(image);
            imageBitmap = (Bitmap)image;
            Filtros.filtromediavizinho(imageBitmap, imgDest);
            pictBoxImg2.Image = imgDest;
        }

        private void SobelcmDMA_Click(object sender, EventArgs e)
        {
            Bitmap imgDest = new Bitmap(image);
            imageBitmap = (Bitmap)image;
            Filtros.generate_lines_SobelDMA(imageBitmap, imgDest);
            pictBoxImg2.Image = imgDest;
        }

        private void PrewittcmDMA_Click(object sender, EventArgs e)
        {
            Bitmap imgDest = new Bitmap(image);
            imageBitmap = (Bitmap)image;
            Filtros.generate_lines_PrewittDMA(imageBitmap, imgDest);
            pictBoxImg2.Image = imgDest;
        }

        private void RobertscmDMA_Click(object sender, EventArgs e)
        {
            Bitmap imgDest = new Bitmap(image);
            imageBitmap = (Bitmap)image;
            Filtros.generate_lines_RobertsDMA(imageBitmap, imgDest);
            pictBoxImg2.Image = imgDest;
        }

        private void MediaKVizinhos9_Click(object sender, EventArgs e)
        {
            Bitmap imgDest = new Bitmap(image);
            imageBitmap = (Bitmap)image;
            Filtros.filtroKVizinhos9(imageBitmap, imgDest);
            pictBoxImg2.Image = imgDest;
        }

        private void Mediana5X5_Click(object sender, EventArgs e)
        {
            Bitmap imgDest = new Bitmap(image);
            imageBitmap = (Bitmap)image;
            Filtros.filtromediana(imageBitmap, imgDest);
            pictBoxImg2.Image = imgDest;
        }

        private void AfinamentoZSsmDMA_Click(object sender, EventArgs e)
        {
            pictBoxImg2.Image = (Bitmap)Filtros.afinamento(pictBoxImg1.Image);
        }

        private void AfinamentoZScmDMA_Click(object sender, EventArgs e)
        {
            pictBoxImg2.Image = (Bitmap)Filtros.afinamento(pictBoxImg1.Image);
        }
    }
}
