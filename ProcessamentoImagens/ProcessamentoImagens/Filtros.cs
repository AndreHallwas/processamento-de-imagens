﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Forms;

namespace ProcessamentoImagens
{
    class Filtros
    {
        static private bool flag;
        static public Image afinamentoDMA(Image img)
        {
            Bitmap result = new Bitmap(img);
            Bitmap image = (Bitmap)img;

            int width = image.Width;
            int height = image.Height;
            byte r, g, b;

            //lock dados bitmap origem
            BitmapData bitmapDataSrc = image.LockBits(new Rectangle(0, 0, width, height),
                ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
            BitmapData bitmapDataDest = result.LockBits(new Rectangle(0, 0, width, height),
                ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

            Color[] ac = new Color[9];
            int stride = bitmapDataSrc.Stride;

            try
            {
                unsafe
                {
                    for (int i = 2; i < image.Height - 2; i++)
                    {
                        for (int j = 3; j < image.Width - 2; j++)
                        {
                            byte* ptr;

                            //pega os 9 pixels em volta do pixel questionado
                            ptr = (byte*)bitmapDataSrc.Scan0.ToPointer();
                            ptr += (i * stride) + (j * 3);
                            b = *(ptr++);
                            g = *(ptr++);
                            r = *(ptr++);
                            ac[0] = Color.FromArgb(r, g, b);

                            ptr = (byte*)bitmapDataSrc.Scan0.ToPointer();
                            ptr += (i * stride) + ((j - 1) * 3);
                            b = *(ptr++);
                            g = *(ptr++);
                            r = *(ptr++);
                            ac[1] = Color.FromArgb(r, g, b);

                            ptr = (byte*)bitmapDataSrc.Scan0.ToPointer();
                            ptr += ((i + 1) * stride) + ((j - 1) * 3);
                            b = *(ptr++);
                            g = *(ptr++);
                            r = *(ptr++);
                            ac[2] = Color.FromArgb(r, g, b);

                            ptr = (byte*)bitmapDataSrc.Scan0.ToPointer();
                            ptr += ((i + 1) * stride) + (j * 3);
                            b = *(ptr++);
                            g = *(ptr++);
                            r = *(ptr++);
                            ac[3] = Color.FromArgb(r, g, b);

                            ptr = (byte*)bitmapDataSrc.Scan0.ToPointer();
                            ptr += ((i + 1) * stride) + ((j + 1) * 3);
                            b = *(ptr++);
                            g = *(ptr++);
                            r = *(ptr++);
                            ac[4] = Color.FromArgb(r, g, b);

                            ptr = (byte*)bitmapDataSrc.Scan0.ToPointer();
                            ptr += (i * stride) + ((j + 1) * 3);
                            b = *(ptr++);
                            g = *(ptr++);
                            r = *(ptr++);
                            ac[5] = Color.FromArgb(r, g, b);

                            ptr = (byte*)bitmapDataSrc.Scan0.ToPointer();
                            ptr += ((i - 1) * stride) + ((j + 1) * 3);
                            b = *(ptr++);
                            g = *(ptr++);
                            r = *(ptr++);
                            ac[6] = Color.FromArgb(r, g, b);

                            ptr = (byte*)bitmapDataSrc.Scan0.ToPointer();
                            ptr += ((i - 1) * stride) + (j * 3);
                            b = *(ptr++);
                            g = *(ptr++);
                            r = *(ptr++);
                            ac[7] = Color.FromArgb(r, g, b);

                            ptr = (byte*)bitmapDataSrc.Scan0.ToPointer();
                            ptr += ((i - 1) * stride) + ((j - 1) * 3);
                            b = *(ptr++);
                            g = *(ptr++);
                            r = *(ptr++);
                            ac[8] = Color.FromArgb(r, g, b);

                            if (RetirarPixel1Parte(ac[0], ac[1], ac[2], ac[3], ac[4], ac[5], ac[6], ac[7], ac[8]))//devo retirar o pixel?
                            {
                                if (!flag)//se a flag não estiver executa condição 3 e 4 do caso 1
                                {
                                    if (Checa3CondicaoCaso1(ac[1], ac[3], ac[7]) && Checa4CondicaoCaso1(ac[1], ac[5], ac[7]))
                                    {
                                        if (!flag)//caso a verificação 4 não tenha ativado a flag
                                        {
                                            byte* ptr2 = (byte*)bitmapDataDest.Scan0.ToPointer();
                                            r = g = b = (byte)255;

                                            ptr2 += (i * stride) + (j * 3);

                                            *(ptr2++) = b;
                                            *(ptr2++) = g;
                                            *(ptr2++) = r;//se sim coloca branco
                                        }
                                        /*else
                                        {
                                            b2.SetPixel(i, j - 1, Color.FromArgb(255, 255, 255));
                                            b2.SetPixel(i, j + 1, Color.FromArgb(255, 255, 255));
                                            b2.SetPixel(i - 1, j, Color.FromArgb(255, 255, 255));
                                            b2.SetPixel(i, j, Color.FromArgb(255, 255, 255));//se sim coloca branco
                                        }*/
                                    }
                                }
                                else//estiver ativada executa a condição 3 e 4 do caso 2
                                {
                                    if (Checa3CondicaoCaso2(ac[1], ac[3], ac[5]) && Checa4CondicaoCaso2(ac[3], ac[5], ac[7]))
                                    {
                                        if (!flag)
                                        {
                                            byte* ptr2 = (byte*)bitmapDataDest.Scan0.ToPointer();
                                            r = g = b = (byte)255;

                                            ptr2 += (i * stride) + (j * 3);

                                            *(ptr2++) = b;
                                            *(ptr2++) = g;
                                            *(ptr2++) = r;
                                        }
                                        else
                                        {
                                            /*b2.SetPixel(i + 1, j, Color.FromArgb(255, 255, 255));
                                            b2.SetPixel(i, j + 1, Color.FromArgb(255, 255, 255));
                                            b2.SetPixel(i - 1, j, Color.FromArgb(255, 255, 255));
                                            b2.SetPixel(i, j, Color.FromArgb(255, 255, 255));*/
                                        }
                                    }
                                }
                            }

                        }
                    }
                }
                //unlock imagem origem
                image.UnlockBits(bitmapDataSrc);
                result.UnlockBits(bitmapDataDest);

                return (Image)result;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            return null;
        }
        static public Image afinamento(Image img)
        {
            Bitmap b = (Bitmap)img;
            Bitmap b2 = b;
            Color[] ac = new Color[9];
            flag = false;
            try
            {
                for (int i = 2; i < b.Width - 2; i++)
                {
                    for (int j = 2; j < b.Height - 2; j++)
                    {
                        //pega os 9 pixels em volta do pixel questionado
                        ac[0] = b.GetPixel(i, j);
                        ac[1] = b.GetPixel(i, j - 1);
                        ac[2] = b.GetPixel(i + 1, j - 1);
                        ac[3] = b.GetPixel(i + 1, j);
                        ac[4] = b.GetPixel(i + 1, j + 1);
                        ac[5] = b.GetPixel(i, j + 1);
                        ac[6] = b.GetPixel(i - 1, j + 1);
                        ac[7] = b.GetPixel(i - 1, j);
                        ac[8] = b.GetPixel(i - 1, j - 1);
                        if (RetirarPixel1Parte(ac[0], ac[1], ac[2], ac[3], ac[4], ac[5], ac[6], ac[7], ac[8]))//devo retirar o pixel?
                        {
                            if(!flag)//se a flag não estiver executa condição 3 e 4 do caso 1
                            {
                                if(Checa3CondicaoCaso1(ac[1], ac[3], ac[7]) && Checa4CondicaoCaso1(ac[1], ac[5], ac[7]))
                                {
                                    if(!flag)//caso a verificação 4 não tenha ativado a flag
                                    {
                                        b2.SetPixel(i, j, Color.FromArgb(255, 255, 255));//se sim coloca branco
                                    } 
                                    /*else
                                    {
                                        b2.SetPixel(i, j - 1, Color.FromArgb(255, 255, 255));
                                        b2.SetPixel(i, j + 1, Color.FromArgb(255, 255, 255));
                                        b2.SetPixel(i - 1, j, Color.FromArgb(255, 255, 255));
                                        b2.SetPixel(i, j, Color.FromArgb(255, 255, 255));//se sim coloca branco
                                    }*/
                                }
                            }
                            else//estiver ativada executa a condição 3 e 4 do caso 2
                            {
                                if(Checa3CondicaoCaso2(ac[1], ac[3], ac[5]) && Checa4CondicaoCaso2(ac[3], ac[5], ac[7]))
                                {
                                    if(!flag)
                                    {
                                        b2.SetPixel(i, j, Color.FromArgb(255, 255, 255));
                                    }
                                    else
                                    {
                                        /*b2.SetPixel(i + 1, j, Color.FromArgb(255, 255, 255));
                                        b2.SetPixel(i, j + 1, Color.FromArgb(255, 255, 255));
                                        b2.SetPixel(i - 1, j, Color.FromArgb(255, 255, 255));
                                        b2.SetPixel(i, j, Color.FromArgb(255, 255, 255));*/
                                    }
                                }
                            }
                        }

                    }
                }
                return (Image)b2;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            return null;
        }
        static private bool Checa1Condicao(Color p1, Color p2, Color p3, Color p4, Color p5, Color p6, Color p7, Color p8, Color p9)
        {
            int c = 0, i = 0;
            Color[] ac = new Color[8];
            ac[0] = p2;
            ac[1] = p3;
            ac[2] = p4;
            ac[3] = p5;
            ac[4] = p6;
            ac[5] = p7;
            ac[6] = p8;
            ac[7] = p9;
            for (i = 0; i < 7; i++)
            {
                if (ac[i] == Color.FromArgb(255, 255, 255) && ac[i + 1] == Color.FromArgb(0, 0, 0))
                    c++;//numero de vezes que a cor transidiu de branco para preto
            }
            if (ac[7] == Color.FromArgb(255, 255, 255) && ac[0] == Color.FromArgb(0, 0, 0))//não sei se deve fazer isso
                c++;
            return c == 1;
        }
        static private bool Checa2Condicao(Color p1, Color p2, Color p3, Color p4, Color p5, Color p6, Color p7, Color p8, Color p9)
        {
            int c = 0;
            Color[] ac = new Color[8];
            ac[0] = p2;
            ac[1] = p3;
            ac[2] = p4;
            ac[3] = p5;
            ac[4] = p6;
            ac[5] = p7;
            ac[6] = p8;
            ac[7] = p9;
            for (int i = 0; i < 8; i++)
                if (ac[i] == Color.FromArgb(0, 0, 0))
                    c++;
            return c >= 2 && c <= 6;
        }
        static private bool Checa3CondicaoCaso1(Color p2, Color p4, Color p8)
        {
            return p2 == Color.FromArgb(255, 255, 255) || p8 == Color.FromArgb(255, 255, 255) || p4 == Color.FromArgb(255, 255, 255);
        }
        static private bool Checa4CondicaoCaso1(Color p2, Color p6, Color p8)
        {
            if (p2 == Color.FromArgb(255, 255, 255) || p6 == Color.FromArgb(255, 255, 255) || p8 == Color.FromArgb(255, 255, 255))
            {
                flag = true;
                return true;
            }
            return false;
        }

        static private bool Checa3CondicaoCaso2(Color p2, Color p4, Color p6)
        {
            return p2 == Color.FromArgb(255, 255, 255) || p6 == Color.FromArgb(255, 255, 255) || p4 == Color.FromArgb(255, 255, 255);
        }
        static private bool Checa4CondicaoCaso2(Color p4, Color p6, Color p8)
        {
            if (p4 == Color.FromArgb(255, 255, 255) || p6 == Color.FromArgb(255, 255, 255) || p8 == Color.FromArgb(255, 255, 255))
            {
                flag = false;
                return true;
            }
            return false;
        }

        static private bool RetirarPixel1Parte(Color p1, Color p2, Color p3, Color p4, Color p5, Color p6, Color p7, Color p8, Color p9)
        {
            return Checa1Condicao(p1, p2, p3, p4, p5, p6, p7, p8, p9) &&
                       Checa2Condicao(p1, p2, p3, p4, p5, p6, p7, p8, p9);

        }
        public static void generate_lines_SobelDMA(Bitmap imageBitmapSrc, Bitmap imageBitmapDest)
        {
            int width = imageBitmapSrc.Width;
            int height = imageBitmapSrc.Height;
            byte r, g, b;
            Int32 r1, b1, g1;
            //lock dados bitmap origem
            BitmapData bitmapDataSrc = imageBitmapSrc.LockBits(new Rectangle(0, 0, width, height),
                ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
            BitmapData bitmapDataDest = imageBitmapDest.LockBits(new Rectangle(0, 0, width, height),
                ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

            int stride = bitmapDataSrc.Stride;

            unsafe
            {
               
                for (int y = 0; y < height; y++)
                {
                    for (int x = 0; x < width; x++)
                    {
                        //obtendo a cor do pixel
                        byte* ptr = (byte*)bitmapDataDest.Scan0.ToPointer();
                        if (x == 0 || x == width - 1 || y == 0 || y == height - 1)
                        {
                            r1 = g1 = b1 = 0; // Image boundary cleared   
                        }
                        else
                        {

                            Color n1;
                            ptr = (byte*)bitmapDataSrc.Scan0.ToPointer();
                            ptr += ((y - 1) * stride) + ((x + 1) * 3);
                            b = *(ptr++);
                            g = *(ptr++);
                            r = *(ptr++);
                            n1 = Color.FromArgb(r, g, b);

                            Color n2;
                            ptr = (byte*)bitmapDataSrc.Scan0.ToPointer();
                            ptr += (y * stride) + ((x + 1) * 3);
                            b = *(ptr++);
                            g = *(ptr++);
                            r = *(ptr++);
                            n2 = Color.FromArgb(r, g, b);

                            Color n3;
                            ptr = (byte*)bitmapDataSrc.Scan0.ToPointer();
                            ptr += ((y + 1) * stride) + ((x + 1) * 3);
                            b = *(ptr++);
                            g = *(ptr++);
                            r = *(ptr++);
                            n3 = Color.FromArgb(r, g, b);

                            Color n4;
                            ptr = (byte*)bitmapDataSrc.Scan0.ToPointer();
                            ptr += ((y - 1) * stride) + ((x - 1) * 3);
                            b = *(ptr++);
                            g = *(ptr++);
                            r = *(ptr++);
                            n4 = Color.FromArgb(r, g, b);

                            Color n5;
                            ptr = (byte*)bitmapDataSrc.Scan0.ToPointer();
                            ptr += (y * stride) + ((x - 1) * 3);
                            b = *(ptr++);
                            g = *(ptr++);
                            r = *(ptr++);
                            n5 = Color.FromArgb(r, g, b);

                            Color n6;
                            ptr = (byte*)bitmapDataSrc.Scan0.ToPointer();
                            ptr += ((y + 1) * stride) + ((x - 1) * 3);
                            b = *(ptr++);
                            g = *(ptr++);
                            r = *(ptr++);
                            n6 = Color.FromArgb(r, g, b);

                            Color k1;
                            ptr = (byte*)bitmapDataSrc.Scan0.ToPointer();
                            ptr += ((y + 1) * stride) + ((x - 1) * 3);
                            b = *(ptr++);
                            g = *(ptr++);
                            r = *(ptr++);
                            k1 = Color.FromArgb(r, g, b);

                            Color k2;
                            ptr = (byte*)bitmapDataSrc.Scan0.ToPointer();
                            ptr += ((y + 1) * stride) + (x * 3);
                            b = *(ptr++);
                            g = *(ptr++);
                            r = *(ptr++);
                            k2 = Color.FromArgb(r, g, b);

                            Color k3;
                            ptr = (byte*)bitmapDataSrc.Scan0.ToPointer();
                            ptr += ((y + 1) * stride) + ((x + 1) * 3);
                            b = *(ptr++);
                            g = *(ptr++);
                            r = *(ptr++);
                            k3 = Color.FromArgb(r, g, b);

                            Color k4;
                            ptr = (byte*)bitmapDataSrc.Scan0.ToPointer();
                            ptr += ((y - 1) * stride) + ((x - 1) * 3);
                            b = *(ptr++);
                            g = *(ptr++);
                            r = *(ptr++);
                            k4 = Color.FromArgb(r, g, b);

                            Color k5;
                            ptr = (byte*)bitmapDataSrc.Scan0.ToPointer();
                            ptr += ((y - 1) * stride) + (x * 3);
                            b = *(ptr++);
                            g = *(ptr++);
                            r = *(ptr++);
                            k5 = Color.FromArgb(r, g, b);

                            Color k6;
                            ptr = (byte*)bitmapDataSrc.Scan0.ToPointer();
                            ptr += ((y - 1) * stride) + ((x + 1) * 3);
                            b = *(ptr++);
                            g = *(ptr++);
                            r = *(ptr++);
                            k6 = Color.FromArgb(r, g, b);

                            r1 = (Int32)MascaraSobel(n1.R, n2.R, n3.R, n4.R, n5.R, n6.R, k1.R, k2.R, k3.R, k4.R, k5.R, k6.R);
                            g1 = (Int32)MascaraSobel(n1.G, n2.G, n3.G, n4.G, n5.G, n6.G, k1.G, k2.G, k3.G, k4.G, k5.G, k6.G);
                            b1 = (Int32)MascaraSobel(n1.B, n2.B, n3.B, n4.B, n5.B, n6.B, k1.B, k2.B, k3.B, k4.B, k5.B, k6.B);
                        }
                        //nova cor
                        /////r1 = g1 = b1 = (Int32)(r1 * 0.2990 + g1 * 0.5870 + b1 * 0.1140);
                        r = (byte)r1;
                        g = (byte)g1;
                        b = (byte)b1;
                        if (r1 > 255)
                        {
                            r = (byte)255;
                        }
                        if (b1 > 255)
                        {
                            b = (byte)255;
                        }
                        if (g1 > 255)
                        {
                            g = (byte)255;
                        }
                        ptr = (byte*)bitmapDataDest.Scan0.ToPointer();
                        ptr += (y * stride) + (x * 3);
                        *(ptr++) = b;
                        *(ptr++) = g;
                        *(ptr++) = r;
                    }
                }
            }
            //unlock imagem origem
            imageBitmapSrc.UnlockBits(bitmapDataSrc);
            imageBitmapDest.UnlockBits(bitmapDataDest);
        }
        public static void generate_lines_RobertsDMA(Bitmap imageBitmapSrc, Bitmap imageBitmapDest)
        {
            int width = imageBitmapSrc.Width;
            int height = imageBitmapSrc.Height;
            byte r, g, b;
            Int32 r1, b1, g1;

            //lock dados bitmap origem
            BitmapData bitmapDataSrc = imageBitmapSrc.LockBits(new Rectangle(0, 0, width, height),
                ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
            BitmapData bitmapDataDest = imageBitmapDest.LockBits(new Rectangle(0, 0, width, height),
                ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

            int stride = bitmapDataSrc.Stride;

            unsafe
            {
                for (int y = 0; y < height; y++)
                {
                    for (int x = 0; x < width; x++)
                    {
                        byte* ptr = (byte*)bitmapDataSrc.Scan0.ToPointer();
                        if (x == 0 || x == width - 1 || y == 0 || y == height - 1)
                        {
                            r1 = g1 = b1 = 0; // Image boundary cleared   
                        }
                        else
                        {

                            Color n1;
                            ptr = (byte*)bitmapDataSrc.Scan0.ToPointer();
                            ptr += (y * stride) + (x * 3);
                            b = *(ptr++);
                            g = *(ptr++);
                            r = *(ptr++);
                            n1 = Color.FromArgb(r, g, b);

                            Color k1;
                            ptr = (byte*)bitmapDataSrc.Scan0.ToPointer();
                            ptr += ((y - 1) * stride) + ((x + 1) * 3);
                            b = *(ptr++);
                            g = *(ptr++);
                            r = *(ptr++);
                            k1 = Color.FromArgb(r, g, b);

                            r1 = (Int32)MascaraRoberts(n1.R, k1.R);
                            g1 = (Int32)MascaraRoberts(n1.R, k1.R);
                            b1 = (Int32)MascaraRoberts(n1.R, k1.R);
                        }
                        //nova cor
                        /////r = g = b = (Int32)(r * 0.2990 + g * 0.5870 + b * 0.1140);
                        r = (byte)r1;
                        g = (byte)g1;
                        b = (byte)b1;
                        if (r1 > 255)
                        {
                            r = (byte)255;
                        }
                        if (b1 > 255)
                        {
                            b = (byte)255;
                        }
                        if (g1 > 255)
                        {
                            g = (byte)255;
                        }
                        ptr = (byte*)bitmapDataDest.Scan0.ToPointer();
                        ptr += (y * stride) + (x * 3);
                        *(ptr++) = b;
                        *(ptr++) = g;
                        *(ptr++) = r;
                    }
                }
            }
            //unlock imagem origem
            imageBitmapSrc.UnlockBits(bitmapDataSrc);
            imageBitmapDest.UnlockBits(bitmapDataDest);
        }
        public static void generate_lines_PrewittDMA(Bitmap imageBitmapSrc, Bitmap imgDest)
        {

            int width = imageBitmapSrc.Width;
            int height = imageBitmapSrc.Height;
            ///imgDest = new Bitmap(width, height);
            byte r, g, b;
            Int32 r1 = 0, g1 = 0, b1 = 0;

            //lock dados bitmap origem
            BitmapData bitmapDataSrc = imageBitmapSrc.LockBits(new Rectangle(0, 0, width, height),
                ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
            BitmapData bitmapDataDest = imgDest.LockBits(new Rectangle(0, 0, width, height),
                ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

            int stride = bitmapDataSrc.Stride;

            unsafe
            {
                for (int y = 0; y < height; y++)
                {
                    for (int x = 0; x < width; x++)
                    {


                        byte* ptr;
                        if (x == 0 || x == width - 1 || y == 0 || y == height - 1)
                        {
                            r = g = b = 0; // Image boundary cleared   
                        }
                        else
                        {
                            //obtendo a cor do pixel

                            Color n1;
                            ptr = (byte*)bitmapDataSrc.Scan0.ToPointer();
                            ptr += ((y - 1) * stride) + ((x + 1) * 3);
                            b = *(ptr++);
                            g = *(ptr++);
                            r = *(ptr++);
                            n1 = Color.FromArgb(r, g, b);

                            Color n2;
                            ptr = (byte*)bitmapDataSrc.Scan0.ToPointer();
                            ptr += (y * stride) + ((x + 1) * 3);
                            b = *(ptr++);
                            g = *(ptr++);
                            r = *(ptr++);
                            n2 = Color.FromArgb(r, g, b);

                            Color n3;
                            ptr = (byte*)bitmapDataSrc.Scan0.ToPointer();
                            ptr += ((y + 1) * stride) + ((x + 1) * 3);
                            b = *(ptr++);
                            g = *(ptr++);
                            r = *(ptr++);
                            n3 = Color.FromArgb(r, g, b);

                            Color n4;
                            ptr = (byte*)bitmapDataSrc.Scan0.ToPointer();
                            ptr += ((y - 1) * stride) + ((x - 1) * 3);
                            b = *(ptr++);
                            g = *(ptr++);
                            r = *(ptr++);
                            n4 = Color.FromArgb(r, g, b);

                            Color n5;
                            ptr = (byte*)bitmapDataSrc.Scan0.ToPointer();
                            ptr += (y * stride) + ((x - 1) * 3);
                            b = *(ptr++);
                            g = *(ptr++);
                            r = *(ptr++);
                            n5 = Color.FromArgb(r, g, b);

                            Color n6;
                            ptr = (byte*)bitmapDataSrc.Scan0.ToPointer();
                            ptr += ((y + 1) * stride) + ((x - 1) * 3);
                            b = *(ptr++);
                            g = *(ptr++);
                            r = *(ptr++);
                            n6 = Color.FromArgb(r, g, b);

                            Color k1;
                            ptr = (byte*)bitmapDataSrc.Scan0.ToPointer();
                            ptr += ((y + 1) * stride) + ((x - 1) * 3);
                            b = *(ptr++);
                            g = *(ptr++);
                            r = *(ptr++);
                            k1 = Color.FromArgb(r, g, b);

                            Color k2;
                            ptr = (byte*)bitmapDataSrc.Scan0.ToPointer();
                            ptr += ((y + 1) * stride) + (x * 3);
                            b = *(ptr++);
                            g = *(ptr++);
                            r = *(ptr++);
                            k2 = Color.FromArgb(r, g, b);

                            Color k3;
                            ptr = (byte*)bitmapDataSrc.Scan0.ToPointer();
                            ptr += ((y + 1) * stride) + ((x + 1) * 3);
                            b = *(ptr++);
                            g = *(ptr++);
                            r = *(ptr++);
                            k3 = Color.FromArgb(r, g, b);

                            Color k4;
                            ptr = (byte*)bitmapDataSrc.Scan0.ToPointer();
                            ptr += ((y - 1) * stride) + ((x - 1) * 3);
                            b = *(ptr++);
                            g = *(ptr++);
                            r = *(ptr++);
                            k4 = Color.FromArgb(r, g, b);

                            Color k5;
                            ptr = (byte*)bitmapDataSrc.Scan0.ToPointer();
                            ptr += ((y - 1) * stride) + (x * 3);
                            b = *(ptr++);
                            g = *(ptr++);
                            r = *(ptr++);
                            k5 = Color.FromArgb(r, g, b);

                            Color k6;
                            ptr = (byte*)bitmapDataSrc.Scan0.ToPointer();
                            ptr += ((y - 1) * stride) + ((x + 1) * 3);
                            b = *(ptr++);
                            g = *(ptr++);
                            r = *(ptr++);
                            k6 = Color.FromArgb(r, g, b);

                            r1 = (Int32)MascaraPrewitt(n1.R, n2.R, n3.R, n4.R, n5.R, n6.R, k1.R, k2.R, k3.R, k4.R, k5.R, k6.R);
                            g1 = (Int32)MascaraPrewitt(n1.G, n2.G, n3.G, n4.G, n5.G, n6.G, k1.G, k2.G, k3.G, k4.G, k5.G, k6.G);
                            b1 = (Int32)MascaraPrewitt(n1.B, n2.B, n3.B, n4.B, n5.B, n6.B, k1.B, k2.B, k3.B, k4.B, k5.B, k6.B);
                        }
                        //nova cor
                        /////r = g = b = (Int32)(r * 0.2990 + g * 0.5870 + b * 0.1140);
                        r = (byte)r1;
                        g = (byte)g1;
                        b = (byte)b1;
                        if (r1 > 255)
                        {
                            r = (byte)255;
                        }
                        if (b1 > 255)
                        {
                            b = (byte)255;
                        }
                        if (g1 > 255)
                        {
                            g = (byte)255;
                        }
                        ptr = (byte*)bitmapDataDest.Scan0.ToPointer();
                        ptr += (y * stride) + (x * 3);
                        *(ptr++) = b;
                        *(ptr++) = g;
                        *(ptr++) = r;
                    }
                }
            }

            //unlock imagem origem
            imageBitmapSrc.UnlockBits(bitmapDataSrc);
            imgDest.UnlockBits(bitmapDataDest);
        }
        //com acesso direito a memoria
        public static void convertCamandas(Bitmap image)
        {
            int width = image.Width;
            int height = image.Height;
            int pixelSize = 3;

            Bitmap vetImage1 = new Bitmap(width, height);
            Bitmap vetImage2 = new Bitmap(width, height);
            Bitmap vetImage3 = new Bitmap(width, height);
            Bitmap vetImage4 = new Bitmap(width, height);
            Bitmap vetImage5 = new Bitmap(width, height);
            Bitmap vetImage6 = new Bitmap(width, height);
            Bitmap vetImage7 = new Bitmap(width, height);
            Bitmap vetImage8 = new Bitmap(width, height);

            //lock dados bitmap origem 
            BitmapData bitmapDataSrc = image.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
            //lock dados bitmap destino
            BitmapData bitmapDataDst1 = vetImage1.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
            BitmapData bitmapDataDst2 = vetImage2.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
            BitmapData bitmapDataDst3 = vetImage3.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
            BitmapData bitmapDataDst4 = vetImage4.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
            BitmapData bitmapDataDst5 = vetImage5.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
            BitmapData bitmapDataDst6 = vetImage6.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
            BitmapData bitmapDataDst7 = vetImage7.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
            BitmapData bitmapDataDst8 = vetImage8.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);


            int padding = bitmapDataSrc.Stride - width;

            unsafe
            {
                byte* ptr = (byte*)bitmapDataSrc.Scan0.ToPointer();

                byte* vetPtr1 = (byte*)bitmapDataDst1.Scan0.ToPointer();
                byte* vetPtr2 = (byte*)bitmapDataDst2.Scan0.ToPointer();
                byte* vetPtr3 = (byte*)bitmapDataDst3.Scan0.ToPointer();
                byte* vetPtr4 = (byte*)bitmapDataDst4.Scan0.ToPointer();
                byte* vetPtr5 = (byte*)bitmapDataDst5.Scan0.ToPointer();
                byte* vetPtr6 = (byte*)bitmapDataDst6.Scan0.ToPointer();
                byte* vetPtr7 = (byte*)bitmapDataDst7.Scan0.ToPointer();
                byte* vetPtr8 = (byte*)bitmapDataDst8.Scan0.ToPointer();


                byte r, g, b;

                for (int y = 0; y < height; y++)
                {
                    for (int x = 0; x < width; x++)
                    {

                        ptr = (byte*)bitmapDataSrc.Scan0.ToPointer();
                        ptr += (padding * y) + (x * 3);

                        vetPtr1 = (byte*)bitmapDataDst1.Scan0.ToPointer();
                        vetPtr1 += (padding * y) + (x * 3);

                        vetPtr2 = (byte*)bitmapDataDst2.Scan0.ToPointer();
                        vetPtr2 += (padding * y) + (x * 3);

                        vetPtr3 = (byte*)bitmapDataDst3.Scan0.ToPointer();
                        vetPtr3 += (padding * y) + (x * 3);

                        vetPtr4 = (byte*)bitmapDataDst4.Scan0.ToPointer();
                        vetPtr4 += (padding * y) + (x * 3);

                        vetPtr5 = (byte*)bitmapDataDst5.Scan0.ToPointer();
                        vetPtr5 += (padding * y) + (x * 3);

                        vetPtr6 = (byte*)bitmapDataDst6.Scan0.ToPointer();
                        vetPtr6 += (padding * y) + (x * 3);

                        vetPtr7 = (byte*)bitmapDataDst7.Scan0.ToPointer();
                        vetPtr7 += (padding * y) + (x * 3);

                        vetPtr8 = (byte*)bitmapDataDst7.Scan0.ToPointer();
                        vetPtr8 += (padding * y) + (x * 3);


                        b = *(ptr++); //está armazenado dessa forma: b g r 
                        g = *(ptr++);
                        r = *(ptr++);

                        byte gray = (byte)((r + g + b) / 3);
                        byte[] bit = new byte[8];

                        for (int i = 0; i < 8; i++)
                        {
                            bit[i] = (byte)(gray % 2);
                            gray /= 2;
                        }

                        bit[0] *= 128; //arquivo 8
                        bit[1] *= 64;
                        bit[2] *= 32;
                        bit[3] *= 16;
                        bit[4] *= 8;
                        bit[5] *= 4;
                        bit[6] *= 2;
                        bit[7] *= 1; //arquivo 1

                        *(vetPtr1++) = bit[7];
                        *(vetPtr1++) = bit[7];
                        *(vetPtr1++) = bit[7];

                        *(vetPtr2++) = bit[6];
                        *(vetPtr2++) = bit[6];
                        *(vetPtr2++) = bit[6];

                        *(vetPtr3++) = bit[5];
                        *(vetPtr3++) = bit[5];
                        *(vetPtr3++) = bit[5];

                        *(vetPtr4++) = bit[4];
                        *(vetPtr4++) = bit[4];
                        *(vetPtr4++) = bit[4];

                        *(vetPtr5++) = bit[3];
                        *(vetPtr5++) = bit[3];
                        *(vetPtr5++) = bit[3];

                        *(vetPtr6++) = bit[2];
                        *(vetPtr6++) = bit[2];
                        *(vetPtr6++) = bit[2];

                        *(vetPtr7++) = bit[1];
                        *(vetPtr7++) = bit[1];
                        *(vetPtr7++) = bit[1];

                        *(vetPtr8++) = bit[0];
                        *(vetPtr8++) = bit[0];
                        *(vetPtr8++) = bit[0];

                    }
                }
            }
            //unlock imagem origem 
            image.UnlockBits(bitmapDataSrc);
            //unlock imagem destino
            vetImage1.UnlockBits(bitmapDataDst1);
            vetImage2.UnlockBits(bitmapDataDst2);
            vetImage3.UnlockBits(bitmapDataDst3);
            vetImage4.UnlockBits(bitmapDataDst4);
            vetImage5.UnlockBits(bitmapDataDst5);
            vetImage6.UnlockBits(bitmapDataDst6);
            vetImage7.UnlockBits(bitmapDataDst7);

            vetImage1.Save("camada_8.jpeg");
            vetImage2.Save("camada_7.jpeg");
            vetImage3.Save("camada_6.jpeg");
            vetImage4.Save("camada_5.jpeg");
            vetImage5.Save("camada_4.jpeg");
            vetImage6.Save("camada_3.jpeg");
            vetImage7.Save("camada_2.jpeg");
            vetImage8.Save("camada_1.jpeg");
        }
        public static void filtroKVizinhos9(Bitmap imageBitmap, Bitmap imgDest)
        {
            int tamanho = 9;
            int raio = tamanho / 2; //quantidade de casas para fora do centro da mascara
            int centro = (tamanho * tamanho) / 2; //calculo do centro da mascara           
            int lMeio, cMeio;
            int corResultado, corResultado2, corResultado3;
            int alt = imageBitmap.Height - 1;
            int larg = imageBitmap.Width - 1;
            int r, g, b;
            //inicia o percurso pela imagem
            for (int l = 0; l <= alt; l++)
            {
                for (int c = 0; c <= larg; c++)
                {
                    int cont = 0; //contador para auxiliar a organizacao do vetor
                    int[] cores = new int[tamanho * tamanho];//crio um vetor do tamanho da mascara (3x3,5x5...)
                    int[] cores2 = new int[tamanho * tamanho];
                    int[] cores3 = new int[tamanho * tamanho];
                    //inicia o posicionamento da mascara na imagem
                    for (lMeio = 0; lMeio < tamanho; lMeio++)
                    {
                        for (cMeio = 0; cMeio < tamanho; cMeio++)
                        {
                            int linha = lMeio + (l - raio); //linha onde sera retirado a cor
                            int coluna = cMeio + (c - raio); //coluna onde sera retirado a cor

                            if (linha < 0)
                            { //verificacao para dobra de imagem 
                                linha = (alt - (raio + linha)); //se for menor que zero, pego a posicao contraria da linha
                            }
                            if (coluna < 0)
                            { //verificacao para dobra de imagem 
                                coluna = (larg - (raio + coluna)); //se for menor que zero, pego a posicao contraria da coluna 
                            }
                            if (linha > alt)
                            { //verificacao para dobra de imagem 
                                linha = linha - alt;//se for maior que altura, pego a posicao contraria da linha                           
                            }
                            if (coluna > larg)
                            { //verificacao para dobra de imagem 
                                coluna = coluna - larg;//se for maior que largura, pego a posicao contraria da coluna
                            }
                            Color cor1 = imageBitmap.GetPixel(coluna, linha);//pega a cor nas coordenadas
                            r = cor1.R;
                            g = cor1.G;
                            b = cor1.B;
                            cores[cont] = r;
                            cores2[cont] = g;
                            cores3[cont] = g;
                            cont++;
                        }
                    }

                    Color cor = imageBitmap.GetPixel(c, l);//pega a cor nas coordenadas
                    r = cor.R;
                    g = cor.G;
                    b = cor.B;
                    Array.Sort(cores);
                    Array.Sort(cores2);
                    Array.Sort(cores3);

                    corResultado = corM(cores, r, 9);
                    corResultado2 = corM(cores2, g, 9);
                    corResultado3 = corM(cores3, b, 9);
                    Color newcolor = Color.FromArgb(corResultado, corResultado2, corResultado3);
                    imgDest.SetPixel(c, l, newcolor);
                }
            }
        }
        public static int corM(int[] cores, int valor, int tamanho)
        {
            int aux = find(cores, valor);
            int soma = 0;
            if (aux > 1 && aux < tamanho && cores[aux] + cores[aux + 1] <= cores[aux] + cores[aux - 1])
            {
                soma = 0;
                for (int i = 0; i < tamanho; i++)
                {
                    if (aux + i < tamanho)
                        soma += cores[aux + i];
                }
            }
            else
            {
                soma = 0;
                for (int i = 0; i < tamanho; i++)
                {
                    if (aux - i > 0)
                        soma += cores[aux - i];
                }
            }
            return soma / tamanho;
        }

        public static int find(int[] cores, int cor)
        {
            int i;
            for (i = 0; i < cores.Length && cores[i] != cor; i++) { };
            if (cores[i] == cor)
            {
                return i;
            }
            return 0;
        }

        public static void filtromediana(Bitmap imageBitmap, Bitmap imgDest)
        {
            int tamanho = 5;
            int raio = tamanho / 2; //quantidade de casas para fora do centro da mascara
            int centro = (tamanho * tamanho) / 2; //calculo do centro da mascara           
            int lMeio, cMeio;
            int corResultado, corResultado2, corResultado3;
            int alt = imageBitmap.Height - 1;
            int larg = imageBitmap.Width - 1;
            int r, g, b;
            //inicia o percurso pela imagem
            for (int l = 0; l <= alt; l++)
            {
                for (int c = 0; c <= larg; c++)
                {
                    int cont = 0; //contador para auxiliar a organizacao do vetor
                    int[] cores = new int[tamanho * tamanho];//crio um vetor do tamanho da mascara (3x3,5x5...)
                    int[] cores2 = new int[tamanho * tamanho];
                    int[] cores3 = new int[tamanho * tamanho];
                    //inicia o posicionamento da mascara na imagem
                    for (lMeio = 0; lMeio < tamanho; lMeio++)
                    {
                        for (cMeio = 0; cMeio < tamanho; cMeio++)
                        {
                            int linha = lMeio + (l - raio); //linha onde sera retirado a cor
                            int coluna = cMeio + (c - raio); //coluna onde sera retirado a cor

                            if (linha < 0)
                            { //verificacao para dobra de imagem 
                                linha = (alt - (raio + linha)); //se for menor que zero, pego a posicao contraria da linha
                            }
                            if (coluna < 0)
                            { //verificacao para dobra de imagem 
                                coluna = (larg - (raio + coluna)); //se for menor que zero, pego a posicao contraria da coluna 
                            }
                            if (linha > alt)
                            { //verificacao para dobra de imagem 
                                linha = linha - alt;//se for maior que altura, pego a posicao contraria da linha                           
                            }
                            if (coluna > larg)
                            { //verificacao para dobra de imagem 
                                coluna = coluna - larg;//se for maior que largura, pego a posicao contraria da coluna
                            }
                            Color cor = imageBitmap.GetPixel(coluna, linha);//pega a cor nas coordenadas
                            r = cor.R;
                            g = cor.G;
                            b = cor.B;
                            cores[cont] = r;
                            cores2[cont] = g;
                            cores3[cont] = g;
                            cont++;
                        }
                    }

                    Array.Sort(cores);
                    Array.Sort(cores2);
                    Array.Sort(cores3);
                    corResultado = cores[centro];
                    corResultado2 = cores2[centro];
                    corResultado3 = cores3[centro];
                    Color newcolor = Color.FromArgb(corResultado, corResultado2, corResultado3);
                    imgDest.SetPixel(c, l, newcolor);
                }
            }
        }
        


        //sem acesso direto a memoria
        public static void generate_lines(Bitmap imageBitmapSrc, Bitmap imageBitmapDest)
        {
            int width = imageBitmapSrc.Width;
            int height = imageBitmapSrc.Height;
            int r, g, b;
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    //obtendo a cor do pixel
                    Color cor = imageBitmapSrc.GetPixel(x, y);
                    if (x == 0 || x == width - 1 || y == 0 || y == height - 1)
                    {
                        r = g = b = 0; // Image boundary cleared   
                    }
                    else
                    {
                        Color n1 = imageBitmapSrc.GetPixel(x + 1, y - 1);
                        Color n2 = imageBitmapSrc.GetPixel(x + 1, y);
                        Color n3 = imageBitmapSrc.GetPixel(x + 1, y + 1);
                        Color n4 = imageBitmapSrc.GetPixel(x - 1, y - 1);
                        Color n5 = imageBitmapSrc.GetPixel(x - 1, y);
                        Color n6 = imageBitmapSrc.GetPixel(x - 1, y + 1);

                        Color k1 = imageBitmapSrc.GetPixel(x - 1, y + 1);
                        Color k2 = imageBitmapSrc.GetPixel(x, y + 1);
                        Color k3 = imageBitmapSrc.GetPixel(x + 1, y + 1);
                        Color k4 = imageBitmapSrc.GetPixel(x - 1, y - 1);
                        Color k5 = imageBitmapSrc.GetPixel(x, y - 1);
                        Color k6 = imageBitmapSrc.GetPixel(x + 1, y - 1);

                        r = (Int32)MascaraSobel(n1.R, n2.R, n3.R, n4.R, n5.R, n6.R, k1.R, k2.R, k3.R, k4.R, k5.R, k6.R);
                        g = (Int32)MascaraSobel(n1.G, n2.G, n3.G, n4.G, n5.G, n6.G, k1.G, k2.G, k3.G, k4.G, k5.G, k6.G);
                        b = (Int32)MascaraSobel(n1.B, n2.B, n3.B, n4.B, n5.B, n6.B, k1.B, k2.B, k3.B, k4.B, k5.B, k6.B);
                    }
                    //nova cor
                    /////r = g = b = (Int32)(r * 0.2990 + g * 0.5870 + b * 0.1140);
                    if (r > 255)
                    {
                        r = 255;
                    }
                    if (b > 255)
                    {
                        b = 255;
                    }
                    if (g > 255)
                    {
                        g = 255;
                    }
                    Color newcolor = Color.FromArgb(r, g, b);
                    imageBitmapDest.SetPixel(x, y, newcolor);
                }
            }
        }

        public static double MascaraSobel(double n1, double n2, double n3, double n4, double n5,
            double n6, double k1, double k2, double k3, double k4, double k5, double k6)
        {
            double n = n1 + 2 * n2 + n3 - n4 - 2 * n5 - n6;
            double k = k1 + 2 * k2 + k3 - k4 - 2 * k5 - k6;
            double z = Math.Abs(n) + Math.Abs(k);
            return z;
        }
        public static void generate_lines_Prewitt(Bitmap imageBitmapSrc, Bitmap imageBitmapDest)
        {
            int width = imageBitmapSrc.Width;
            int height = imageBitmapSrc.Height;
            int r, g, b;
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    //obtendo a cor do pixel
                    Color cor = imageBitmapSrc.GetPixel(x, y);
                    if (x == 0 || x == width - 1 || y == 0 || y == height - 1)
                    {
                        r = g = b = 0; // Image boundary cleared   
                    }
                    else
                    {
                        Color n1 = imageBitmapSrc.GetPixel(x + 1, y - 1);
                        Color n2 = imageBitmapSrc.GetPixel(x + 1, y);
                        Color n3 = imageBitmapSrc.GetPixel(x + 1, y + 1);
                        Color n4 = imageBitmapSrc.GetPixel(x - 1, y - 1);
                        Color n5 = imageBitmapSrc.GetPixel(x - 1, y);
                        Color n6 = imageBitmapSrc.GetPixel(x - 1, y + 1);

                        Color k1 = imageBitmapSrc.GetPixel(x - 1, y + 1);
                        Color k2 = imageBitmapSrc.GetPixel(x, y + 1);
                        Color k3 = imageBitmapSrc.GetPixel(x + 1, y + 1);
                        Color k4 = imageBitmapSrc.GetPixel(x - 1, y - 1);
                        Color k5 = imageBitmapSrc.GetPixel(x, y - 1);
                        Color k6 = imageBitmapSrc.GetPixel(x + 1, y - 1);

                        r = (Int32)MascaraPrewitt(n1.R, n2.R, n3.R, n4.R, n5.R, n6.R, k1.R, k2.R, k3.R, k4.R, k5.R, k6.R);
                        g = (Int32)MascaraPrewitt(n1.G, n2.G, n3.G, n4.G, n5.G, n6.G, k1.G, k2.G, k3.G, k4.G, k5.G, k6.G);
                        b = (Int32)MascaraPrewitt(n1.B, n2.B, n3.B, n4.B, n5.B, n6.B, k1.B, k2.B, k3.B, k4.B, k5.B, k6.B);
                    }
                    //nova cor
                    /////r = g = b = (Int32)(r * 0.2990 + g * 0.5870 + b * 0.1140);
                    if (r > 255)
                    {
                        r = 255;
                    }
                    if (b > 255)
                    {
                        b = 255;
                    }
                    if (g > 255)
                    {
                        g = 255;
                    }
                    Color newcolor = Color.FromArgb(r, g, b);
                    imageBitmapDest.SetPixel(x, y, newcolor);
                }
            }
        }

        public static double MascaraPrewitt(double n1, double n2, double n3, double n4, double n5,
            double n6, double k1, double k2, double k3, double k4, double k5, double k6)
        {
            double n = n1 + n2 + n3 - n4 - n5 - n6;
            double k = k1 + k2 + k3 - k4 - k5 - k6;
            double z = Math.Abs(n) + Math.Abs(k);
            return z;
        }

        public static void generate_lines_Roberts(Bitmap imageBitmapSrc, Bitmap imageBitmapDest)
        {
            int width = imageBitmapSrc.Width;
            int height = imageBitmapSrc.Height;
            int r, g, b;
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    //obtendo a cor do pixel
                    Color cor = imageBitmapSrc.GetPixel(x, y);
                    if (x == 0 || x == width - 1 || y == 0 || y == height - 1)
                    {
                        r = g = b = 0; // Image boundary cleared   
                    }
                    else
                    {
                        Color n1 = imageBitmapSrc.GetPixel(x, y);

                        Color k1 = imageBitmapSrc.GetPixel(x + 1, y - 1);

                        r = (Int32)MascaraRoberts(n1.R, k1.R);
                        g = (Int32)MascaraRoberts(n1.R, k1.R);
                        b = (Int32)MascaraRoberts(n1.R, k1.R);
                    }
                    //nova cor
                    /////r = g = b = (Int32)(r * 0.2990 + g * 0.5870 + b * 0.1140);
                    if (r > 255)
                    {
                        r = 255;
                    }
                    if (b > 255)
                    {
                        b = 255;
                    }
                    if (g > 255)
                    {
                        g = 255;
                    }
                    Color newcolor = Color.FromArgb(r, g, b);
                    imageBitmapDest.SetPixel(x, y, newcolor);
                }
            }
        }

        public static double MascaraRoberts(double n1, double k1)
        {
            double n = n1;
            double k = k1;
            double z = Math.Abs(n) - Math.Abs(k);
            return Math.Abs(z);
        }

        public static void convert_to_gray(Bitmap imageBitmapSrc, Bitmap imageBitmapDest)
        {
            int width = imageBitmapSrc.Width;
            int height = imageBitmapSrc.Height;
            int r, g, b;
            Int32 gs;

            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    //obtendo a cor do pixel
                    Color cor = imageBitmapSrc.GetPixel(x, y);

                    r = cor.R;
                    g = cor.G;
                    b = cor.B;
                    gs = (Int32)(r * 0.2990 + g * 0.5870 + b * 0.1140);

                    //nova cor
                    Color newcolor = Color.FromArgb(gs, gs, gs);

                    imageBitmapDest.SetPixel(x, y, newcolor);
                }
            }
        }

        //sem acesso direito a memoria
        public static void negativo(Bitmap imageBitmapSrc, Bitmap imageBitmapDest)
        {
            int width = imageBitmapSrc.Width;
            int height = imageBitmapSrc.Height;
            int r, g, b;

            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    //obtendo a cor do pixel
                    Color cor = imageBitmapSrc.GetPixel(x, y);

                    r = cor.R;
                    g = cor.G;
                    b = cor.B;

                    //nova cor
                    Color newcolor = Color.FromArgb(255 - r, 255 - g, 255 - b);

                    imageBitmapDest.SetPixel(x, y, newcolor);
                }
            }
        }

        //com acesso direto a memória
        public static void convert_to_grayDMA(Bitmap imageBitmapSrc, Bitmap imageBitmapDest)
        {
            int width = imageBitmapSrc.Width;
            int height = imageBitmapSrc.Height;
            int pixelSize = 3;
            Int32 gs;

            //lock dados bitmap origem
            BitmapData bitmapDataSrc = imageBitmapSrc.LockBits(new Rectangle(0, 0, width, height),
                ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
            //lock dados bitmap destino
            BitmapData bitmapDataDst = imageBitmapDest.LockBits(new Rectangle(0, 0, width, height),
                ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

            int padding = bitmapDataSrc.Stride - (width * pixelSize);

            unsafe
            {
                byte* src = (byte*)bitmapDataSrc.Scan0.ToPointer();
                byte* dst = (byte*)bitmapDataDst.Scan0.ToPointer();

                int r, g, b;
                for (int y = 0; y < height; y++)
                {
                    for (int x = 0; x < width; x++)
                    {
                        b = *(src++); //está armazenado dessa forma: b g r 
                        g = *(src++);
                        r = *(src++);
                        gs = (Int32)(r * 0.2990 + g * 0.5870 + b * 0.1140);
                        *(dst++) = (byte)gs;
                        *(dst++) = (byte)gs;
                        *(dst++) = (byte)gs;
                    }
                    src += padding;
                    dst += padding;
                }
            }
            //unlock imagem origem
            imageBitmapSrc.UnlockBits(bitmapDataSrc);
            //unlock imagem destino
            imageBitmapDest.UnlockBits(bitmapDataDst);
        }

        //com acesso direito a memoria
        public static void negativoDMA(Bitmap imageBitmapSrc, Bitmap imageBitmapDest)
        {
            int width = imageBitmapSrc.Width;
            int height = imageBitmapSrc.Height;
            int pixelSize = 3;

            //lock dados bitmap origem 
            BitmapData bitmapDataSrc = imageBitmapSrc.LockBits(new Rectangle(0, 0, width, height),
                ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
            //lock dados bitmap destino
            BitmapData bitmapDataDst = imageBitmapDest.LockBits(new Rectangle(0, 0, width, height),
                ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

            int padding = bitmapDataSrc.Stride - (width * pixelSize);

            unsafe
            {
                byte* src1 = (byte*)bitmapDataSrc.Scan0.ToPointer();
                byte* dst = (byte*)bitmapDataDst.Scan0.ToPointer();

                int r, g, b;
                for (int y = 0; y < height; y++)
                {
                    for (int x = 0; x < width; x++)
                    {
                        b = *(src1++); //está armazenado dessa forma: b g r 
                        g = *(src1++);
                        r = *(src1++);

                        *(dst++) = (byte)(255 - b);
                        *(dst++) = (byte)(255 - g);
                        *(dst++) = (byte)(255 - r);
                    }
                    src1 += padding;
                    dst += padding;
                }
            }
            //unlock imagem origem 
            imageBitmapSrc.UnlockBits(bitmapDataSrc);
            //unlock imagem destino
            imageBitmapDest.UnlockBits(bitmapDataDst);
        }



        public static void filtromediavizinho(Bitmap imageBitmap, Bitmap imgDest)
        {
            int tamanho = 5;
            int raio = tamanho / 2; //quantidade de casas para fora do centro da mascara

            int lMeio, cMeio;
            double soma, soma2, soma3;
            byte corResultado, corResultado2, corResultado3;
            int alt = imageBitmap.Height - 1;
            int larg = imageBitmap.Width - 1;
            int r, g, b;
            //inicia o percurso pela imagem
            for (int l = 0; l <= alt; l++)
            {
                for (int c = 0; c <= larg; c++)
                {
                    soma = 0; //variavel para somar valor das cores
                    soma2 = 0;
                    soma3 = 0;
                    for (lMeio = 0; lMeio < tamanho; lMeio++)
                    {
                        for (cMeio = 0; cMeio < tamanho; cMeio++)
                        {
                            int linha = lMeio + (l - raio); //linha onde sera retirado a cor
                            int coluna = cMeio + (c - raio); //coluna onde sera retirado a cor

                            if (linha < 0)
                            { //verificacao para dobra de imagem 
                                linha = (alt - (raio + linha)); //se for menor que zero, pego a posicao contraria da linha
                            }
                            if (coluna < 0)
                            { //verificacao para dobra de imagem 
                                coluna = (larg - (raio + coluna)); //se for menor que zero, pego a posicao contraria da coluna 
                            }
                            if (linha > alt)
                            { //verificacao para dobra de imagem 
                                linha = linha - alt;//se for maior que altura, pego a posicao contraria da linha                           
                            }
                            if (coluna > larg)
                            { //verificacao para dobra de imagem 
                                coluna = coluna - larg;//se for maior que largura, pego a posicao contraria da coluna
                            }

                            Color cor = imageBitmap.GetPixel(coluna, linha);//pega a cor nas coordenadas
                            r = cor.R;
                            g = cor.G;
                            b = cor.B;
                            soma += r;//adiciono a cor ao acumulo das outras cores
                            soma2 += g;
                            soma3 += b;
                        }
                    }

                    corResultado = (byte)(soma / (tamanho * tamanho));//obtenho o resultado dividindo a soma pelo tamanho da mascara
                    corResultado2 = (byte)(soma2 / (tamanho * tamanho));
                    corResultado3 = (byte)(soma3 / (tamanho * tamanho));
                    Color newcolor = Color.FromArgb(corResultado, corResultado2, corResultado3);
                    imgDest.SetPixel(c, l, newcolor);
                }
            }
        }

        //sem acesso a memória
        public static void InverteRandB_sem_memoria(Bitmap imageBitmapSrc, Bitmap imageBitmapDest)
        {
            int width = imageBitmapSrc.Width;
            int height = imageBitmapSrc.Height;
            int r, g, b;

            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    //obtendo a cor do pixel
                    Color cor = imageBitmapSrc.GetPixel(x, y);

                    b = cor.R;
                    g = cor.G;
                    r = cor.B;

                    //nova cor
                    Color newcolor = Color.FromArgb(255 - r, 255 - g, 255 - b);

                    imageBitmapDest.SetPixel(x, y, newcolor);
                }
            }
        }

        public static void InverteRandB_com_memoria(Bitmap imageBitmapSrc, Bitmap imageBitmapDest)
        {
            int width = imageBitmapSrc.Width;
            int height = imageBitmapSrc.Height;
            int pixelSize = 3;

            //lock dados bitmap origem 
            BitmapData bitmapDataSrc = imageBitmapSrc.LockBits(new Rectangle(0, 0, width, height),
                ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
            //lock dados bitmap destino
            BitmapData bitmapDataDst = imageBitmapDest.LockBits(new Rectangle(0, 0, width, height),
                ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

            int padding = bitmapDataSrc.Stride - (width * pixelSize);

            unsafe
            {
                byte* src1 = (byte*)bitmapDataSrc.Scan0.ToPointer();
                byte* dst = (byte*)bitmapDataDst.Scan0.ToPointer();

                int r, g, b;
                for (int y = 0; y < height; y++)
                {
                    for (int x = 0; x < width; x++)
                    {
                        r = *(src1++); //está armazenado dessa forma: b g r 
                        g = *(src1++);
                        b = *(src1++);

                        *(dst++) = (byte)(255 - b);
                        *(dst++) = (byte)(255 - g);
                        *(dst++) = (byte)(255 - r);
                    }
                    src1 += padding;
                    dst += padding;
                }
            }
            //unlock imagem origem 
            imageBitmapSrc.UnlockBits(bitmapDataSrc);
            //unlock imagem destino
            imageBitmapDest.UnlockBits(bitmapDataDst);
        }

        //sem acesso a memória
        public static Bitmap Gira90Grau(Bitmap imageBitmapSrc, Bitmap ImagemRotacionada)
        {
            int width = imageBitmapSrc.Width;
            int height = imageBitmapSrc.Height;
            ImagemRotacionada = new Bitmap(height, width);
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    try
                    {
                        ImagemRotacionada.SetPixel((height - 1) - j, i, imageBitmapSrc.GetPixel(i, j));//imageBitmapSrc.GetPixel(i, j) retorna um Color
                    }
                    catch (Exception exec)
                    {
                        MessageBox.Show(exec.Message);
                    }
                }
            }
            return ImagemRotacionada;
        }
        //com acesso a memória
        public static void Girar90ComMemoria(Bitmap imageBitmapSrc, Bitmap imageBitmapDest)
        {
            int width = imageBitmapSrc.Width;
            int height = imageBitmapSrc.Height;
            int pixelSize = 3;
            Rectangle img1 = new Rectangle(0, 0, width, height);
            Rectangle img2 = new Rectangle(0, 0, height, width);
            //lock dados bitmap origem e depois destino
            BitmapData bitmapDataSrc = imageBitmapSrc.LockBits(img1, ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
            BitmapData bitmapDataDst = imageBitmapDest.LockBits(img2, ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

            int paddingSrc = bitmapDataSrc.Stride - (width * pixelSize);
            int paddingDest = bitmapDataDst.Stride - (width * pixelSize);
            int strideSrc = bitmapDataSrc.Stride;
            int strideDest = bitmapDataDst.Stride;

            byte r, g, b;

            unsafe
            {
                byte* src = (byte*)bitmapDataSrc.Scan0.ToPointer();
                byte* dst = (byte*)bitmapDataDst.Scan0.ToPointer();

                for (int lin = 0; lin < height; lin++)
                    for (int offset = 0; offset < width; offset++)
                    {
                        //pega pixel
                        src = (byte*)bitmapDataSrc.Scan0.ToPointer();
                        src += (lin * strideSrc) + (offset * 3);
                        b = *(src++);
                        g = *(src++);
                        r = *(src++);

                        //altera pixel
                        dst = (byte*)bitmapDataDst.Scan0.ToPointer();
                        dst += (offset * strideDest) + ((height - (lin + 1)) * 3);
                        *(dst++) = b;
                        *(dst++) = g;
                        *(dst++) = r;
                    }
            }
            //desbloqueia imagem destino e origem
            imageBitmapSrc.UnlockBits(bitmapDataSrc);
            imageBitmapDest.UnlockBits(bitmapDataDst);
        }
    }
}
